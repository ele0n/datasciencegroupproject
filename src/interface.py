
from tkinter import *


class Interface(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)

        self.master = master

        self.init_window()

    def init_window(self):
        self.master.title("AirQuality")
        self.master.wm_iconbitmap('images/air_quality.ico')

        files = ['no', 'nox', 'o3', 'so2', 'temp', 'co', 'co2', 'particle_concentration']

        Label(text="FILE:", relief=RIDGE,width=15).grid(row=0, column=0)
        e1 = Entry(self.master, bd = 5)
        e1.grid(row=0, column=1)
        for index, file in enumerate(files):
            if index == len(files)-1:
                e1.insert(len(e1.get()), file)
            else:
                e1.insert(len(e1.get()), file + ", ")

        chk = Checkbutton(self.master, text='scale_data')

        # self.pack(fill=BOTH, expand=1)

        # quitButton = Button(self, text="quit")
        # quitButton.place(x=0, y=0)