from scaler import Scaler
from model import Model
from arima import Arima
from tkinter import *
from interface import Interface
import sys
from ast import literal_eval
import warnings
warnings.filterwarnings("ignore")

if __name__ == '__main__':

    files = ['no', 'nox', 'o3', 'so2', 'temp', 'co', 'co2', 'particle_concentration']

    # root = Tk()
    # root.geometry("400x300")
    # app = Interface(root)

    # root.mainloop()

    # files = ['nox']

    # args: scale, True/False, svr/arima
    if sys.argv[1] == 'scale':
        scaler = Scaler(files)
        # scaler.print_mean_and_variance()

        scaler.get_info(scale=literal_eval(sys.argv[2]))
        scaler.predict_missing_values()
        # scaler.print_result()

    # model.feature_selection()
    if sys.argv[3] == 'svr':
        model = Model(files, c_range=range(5, 10))
        model.build_sets()
        # model.feature_selection()
        model.test(scoring='r2')
        model.compare_algorithms()
    elif sys.argv[2] == 'arima':
        m = Arima(create_dataset=False)
        # m.seasonal_plot()
        # m.evaluate_models([0, 1, 2, 3], [0, 1], [0, 1, 2])
        m.test()

    # model.lasso()

    # model.print_correlation()
