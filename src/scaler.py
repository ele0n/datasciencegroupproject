import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt


class Scaler:

    def __init__(self, files):
        self.files = files

        self.scale_file = []
        for file in files:
            self.scale_file.append((file, file+"_scaled"))

        self.data_file = []
        for file in files:
            self.data_file.append((file, file+"_info"))

    def scale_data(self):
        # files = ['no', 'nox', .... ]
        for file in self.files:
            print("Scaling: ", file)
            df = pd.read_csv("../Data/" + file + "/"+file+".csv", sep='\t')

            if file == "temp":
                file = 'temp_C'

            scale_data = (df[file]-df[file].min())/(df[file].max()-df[file].min())

            print("Start writing")

            df.drop('Unnamed: 0', axis=1, inplace=True)
            df.drop(file, axis=1, inplace=True)
            if file == 'temp_C':
                file = 'temp'
            df[file] = scale_data

            df.to_csv("../Data/"+file+"/"+file+"_scaled.csv", sep='\t', encoding='utf-8')

    def get_info(self, scale=False):
        """
        Take the minimum, maximum, average and amplitude for each data we give in input
        :param scale: if true scale the data
        :return:
        """
        if scale:
            self.scale_data()

        for file in self.scale_file:
            print("Get info: ", file[1])
            df = pd.read_csv("../Data/" + file[0] + "/"+file[1]+".csv", sep='\t')

            new_df = df[[file[0], 'Year', 'Month', 'Day']].groupby(['Year', 'Month', 'Day']).mean()
            new_df = new_df.reset_index()
            new_df = new_df.rename(columns={file[0]: 'Average'})

            df_min = df[[file[0], 'Year', 'Month', 'Day']].groupby(['Year', 'Month', 'Day']).min()
            df_min = df_min.reset_index()
            df_min = df_min.rename(columns={file[0]: 'Min'})

            new_df['Min'] = df_min['Min']

            df_max = df[[file[0], 'Year', 'Month', 'Day']].groupby(['Year', 'Month', 'Day']).max()
            df_max = df_max.reset_index()
            df_max = df_max.rename(columns={file[0]: 'Max'})

            new_df['Max'] = df_max['Max']

            df_variance = df[[file[0], 'Year', 'Month', 'Day']].groupby(['Year', 'Month', 'Day']).var()
            df_variance = df_variance.reset_index()
            df_variance = df_variance.rename(columns={file[0]: 'Variance'})

            new_df['Variance'] = df_variance['Variance']

            # print(new_df)

            new_df.to_csv("../Data/"+file[0]+"/"+file[0]+"_info.csv", sep='\t', encoding='utf-8')

    def print_mean_and_variance(self):
        for file in self.data_file:
            df = pd.read_csv("../Data/"+file[0]+"/"+file[1]+".csv", sep='\t')

            # dates = []
            # for year in range(df.iloc[0]['Year'], df.iloc[-1]['Year']):
            #     for month in range(1, 12):
            #         dates.append(dt.datetime(year=year, month=month, day=1))

            plt.clf()
            plt.plot(np.arange(len(df['Average_interpolate'])), df['Average_interpolate'], label="avg")
            plt.plot(np.arange(len(df['Variance_interpolate'])), df['Variance_interpolate'], label="variance")
            plt.title(file[0])
            plt.legend()
            plt.savefig("../results/images/mean_variance/"+file[0])

    def predict_missing_values(self):

        for file in self.data_file:
            df = pd.read_csv("../Data/"+file[0]+"/"+file[1]+".csv", sep='\t')

            df['Average_interpolate'] = df['Average']
            df['Average_rolling'] = df['Average']
            df['Min_interpolate'] = df['Min']
            df['Min_rolling'] = df['Min']
            df['Max_interpolate'] = df['Max']
            df['Max_rolling'] = df['Max']
            df['Variance_interpolate'] = df['Variance']
            df['Variance_rolling'] = df['Variance']

            df['Average_rolling'] = df.Average_rolling.apply(lambda x: np.nan if pd.isnull(x) else x)
            # print(df['Average_rolling'][44])
            df['Average_rolling'] = df.Average_rolling.fillna(df.Average_rolling.rolling(4, min_periods=1).mean())
            # print(df['Average_rolling'][44])

            df['Min_rolling'] = df.Min_rolling.apply(lambda x: np.nan if pd.isnull(x) else x)
            df['Min_rolling'] = df.Min_rolling.fillna(df.Min_rolling.rolling(4, min_periods=1).mean())

            df['Max_rolling'] = df.Max_rolling.apply(lambda x: np.nan if pd.isnull(x) else x)
            df['Max_rolling'] = df.Max_rolling.fillna(df.Min_rolling.rolling(4, min_periods=1).mean())

            df['Variance_rolling'] = df.Variance_rolling.apply(lambda x: np.nan if pd.isnull(x) else x)
            df['Variance_rolling'] = df.Variance_rolling.fillna(df.Min_rolling.rolling(4, min_periods=1).mean())

            df['Average_interpolate'].interpolate(method='linear', inplace=True)
            df['Min_interpolate'].interpolate(method='linear', inplace=True)
            df['Max_interpolate'].interpolate(method='linear', inplace=True)
            df['Variance_interpolate'].interpolate(method='linear', inplace=True)

            # df['Average_rolling'] = old_df['col']

            # df.shift().rolling(window=4, min_periods=1).mean()
            
            df['Predicted'] = df['Min'] != df['Min_interpolate']

            df.to_csv("../Data/"+file[0]+"/"+file[1]+".csv", sep='\t', encoding='utf-8', float_format='%.6f')

    def print_result(self, _range=range(2005, 2019)):

        for file in self.data_file:

            df = pd.read_csv("../Data/"+file[0]+"/"+file[1]+".csv", sep='\t')

            for n in _range:

                print('print image Year:', n)

                year_df = df.loc[(df['Year'] == n)]

                months = year_df['Month']

                for month in months:

                    month_df = year_df.loc[(df['Month'] == month)]
                    y = month_df['Average']
                    y_min = month_df['Min']
                    y_max = month_df['Max']
                    y_variance = month_df['Variance']
                    y_i = month_df['Average_interpolate']
                    y_min_i = month_df['Min_interpolate']
                    y_max_i = month_df['Max_interpolate']
                    y_variance_i = month_df['Variance_interpolate']
                    y_r = month_df['Average_rolling']
                    y_min_r = month_df['Min_rolling']
                    y_max_r = month_df['Max_rolling']
                    y_variance_r = month_df['Variance_rolling']
                    plt.clf()
                    plt.plot(np.arange(len(month_df)), y, label="Average", color='b')
                    plt.plot(np.arange(len(month_df)), y_min, label="Min", color='r')
                    plt.plot(np.arange(len(month_df)), y_max, label="Max", color='g')
                    plt.plot(np.arange(len(month_df)), y_variance, label="Var", color='c')
                    plt.plot(np.arange(len(month_df)), y_i, '--', color='b')
                    plt.plot(np.arange(len(month_df)), y_r, '--', color='k')
                    plt.plot(np.arange(len(month_df)), y_min_r, '--', color='k')
                    plt.plot(np.arange(len(month_df)), y_max_r, '--', color='k')
                    plt.plot(np.arange(len(month_df)), y_min_i, '--', color='r')
                    plt.plot(np.arange(len(month_df)), y_max_i, '--', color='g')

                    plt.plot(np.arange(len(month_df)), y_variance_r, '--', color='k')
                    plt.plot(np.arange(len(month_df)), y_variance_i, '--', color='c')
                    plt.xlabel("days")
                    plt.ylabel("value")
                    plt.legend(['Average', 'Min', 'Max', 'Var'], loc='upper center', bbox_to_anchor=(0.5, -0.05),
                               fancybox=True, shadow=True, ncol=5)
                    plt.title(file[0]+"-"+str(month)+"-"+str(n))

                    plt.savefig("../Data/images/"+file[0]+"/"+file[0]+"_"+str(n)+"_"+str(month)+".png")


def average_data(df, name):

    # print(df[['Day']].mean(axis=1))

    if name == 'temp':
        name = 'temp_C'

    # df = df.dropna(subset=['avg'])

    # make the average group by Day
    new_df = df[[name, 'Year', 'Month', 'Day']].groupby(['Year', 'Month', 'Day']).mean()

    print(new_df)

    name_file = name+"_average.csv"

    new_df.to_csv("../Data/"+name_file, sep='\t', encoding='utf-8')


def calculate_variance():
    df = pd.read_csv("../Data/" + "no" + "/"+"no_scaled"+".csv", sep='\t')

    values = df.loc[(df['Day'] == 25) & (df['Year'] == 2005) & (df['Month'] == 11)]

    print("Variance", np.var(values['no']))

