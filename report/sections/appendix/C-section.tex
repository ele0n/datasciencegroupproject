\section{Data Analysis}

\subsection{ARIMA}
\label{appendix:C-Arima}

The first model we tried is the \textbf{ARIMA model}. Before analyzing the ARIMA model, it is necessary to define another class of models called ARMA. \\
ARMA is the autoregressive model with moving average and that is a class of stochastic processes that are represented by linear type bonds with their own past (autoregressive component or AR) and with current and past values of a white noise type stochastic component (moving average component or MA). For this reason they are also called mixed models. \\ 

ARMA models are modeled choosing 2 parameters: p and q. p is the number of considered observations before time \texttt{t}, while q is the number of error components used by the model. Each different model can be defined, specifically, with the following formula:

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{images/arma-model.png}
	\label{fig:arma-model}
\end{figure}

Basically, the AR part tells us how many observations of the studied value influence the prediction at time t, while the MA part tells us how many mistakes of the past influence on the same prediction at the same time t.

Besides ARMA models there are different ways to represent the behaviors of the observed time series that the standard ARMA models fail to grasp: ARIMA models can model non-stationary and stationary series with a frequency operator, while SARIMA models are able to model seasonality.

\textbf{Our solution} consists in an ARIMA model because, as we mentioned in the section one, the series of the level of the ozone seems to be non-stationary.

\textbf{ARIMA models} include, in addition to the aforementioned parameters p and q, also a parameter d. If this parameter is set to one, the value at time t is subtracted with the value at time t-1. More specifically, the value $Y_{t}$ in the above formula becomes $Y_{t} - Y_{t-1}$.

\subsubsection{Choosing parameters}

As we said 3 parameters are needed to be chosen to create an ARIMA model. Our idea was to perform a \textbf{GRID search} trying to fit and test different models with different values of p, d and q. Basically, after have decided the values for each p, d and q we fit and test the model with the corresponding parameter. Our first choice was p = [0, 1, 2, 3], d = [0, 1] and q = [0, 1, 2]. The best model chosen by this procedure was ARIMA(p=2, d=0, q=1). The quality of the model was measured taking the best \textbf{Mean Squared Error}.

\subsection{Results}

After setting the model with the best values previously found, we have analyzed the found results using ACF (autocorrelation function) and PACF (partial autocorrelation function). To generate the the two residuals plot we didn't use python, but we used another software called \href{http://gretl.sourceforge.net/}{gretl}.

The results are the following:

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{images/first_arima_result.png}
	\caption{Residual plot - ARIMA}
	\label{fig:first-arima-result}
\end{figure}

Both ACF and PACF residuals suggest that there could be a \textbf{seasonal pattern} in the data. In order to improve the found results, we also tried to use a \textbf{SARIMA model} with the same values of p and q, but this time trying to remove the seasonal component. \\
You can see the results in \ref{fig:second-arima-result} and \ref{fig:third-arima-result}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.55\textwidth]{images/second_arima_result.png}
	\caption{first residual plot - SARIMA}
	\label{fig:second-arima-result}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.55\textwidth]{images/third_arima_result.png}
	\caption{second residual plot - SARIMA}
	\label{fig:third-arima-result}
\end{figure}

As it's shown in the image the results are not better than the previous model. Furthermore, by reducing the number of lags (from 700 to 30), \textbf{another weekly component} can be noticed. As you can see especially in the second chart in \ref{fig:third-arima-result} there is a value that comes out of the band and is repeated every seven observations (which means every seven days).
Despite this new weekly component, we have decided not to continue to develop the model because in this case it is already worse, in terms of MSE, than the ARIMA model previously found. As a result, we used the latter model to make predictions, which are summerized in \ref{fig:arima-result}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\textwidth]{images/ARIMA_result.png}
	\caption{Arima(2,0,1) results}
	\label{fig:arima-result}
\end{figure}

\newpage
\subsection{SVM for regression}
\label{appendix:C-SVR}

The SVR model is trained in the following way:
\\

Regarding the svm for the regression we have done the following steps. Having many initial features we decided to look for the model that best captures the variance of the data. To do this we evaluated the SVM model performing a "step-wise-like" procedure using the $R^2$ as evaluation criterion. This criterion is the coefficient of determination, i.e. the proportion of the variance in the dependent variable (the Average Ozone level) that is predictable from the independent variable(s). \\
This method consists of the following successive steps:
\begin{enumerate}
	\item Search for the best model with one feature;
	\item Search for the best model with two features, constructed upon the previous one;
	\item Iterate.
\end{enumerate}

To make the $R^2$ estimates unbiased, we used the \textbf{cross-validation} approach to both search which and how many features could better capture the variability of the data and finally make the estimate of $R^2$ as close as possible to the real one. To do this, the dataset was divided into 4 folds. The obtained result is shown in figure \ref{fig:rsquaredselection}:

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{images/selection.png}
	\caption{level of $R^2$ according to the number of added variables}
	\label{fig:rsquaredselection}
\end{figure}

As we can see in \ref{fig:rsquaredselection} the generalized model that captures in the most effectful way is the model with 8 features. Adding more features seems to produce an \textbf{overfitting effect} because the capability of the model to generalize starts to get lower. \\
To better understand which features bring a greater benefit to the model we have also plotted the first eight variables selected by the "step-wise-like" procedure. For each of them we have associated an importance value that corresponds to the increase, in terms of explained variance, that the variable incrementally leads to the model. The result is displayed in figure \ref{fig:result-importance}: 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{images/features_importance.png}
	\caption{importance of each selected feature}
	\label{fig:result-importance}
\end{figure}

\newpage
After the feature selection we deal with the problem of setting the hyperparameters of the SVM. Most of them are chosen only changing the value by hand (such as $\epsilon$ and the kernel function, which are corrispectively set to 'rbf' and '0,001'). But the most important parameter $C$ is chosen analyzing the performance of different models, each created with a different C and finally compared among them.
In fact, when considering a regression problem, the idea is to define an $\epsilon$-tube: predictions which differs from the desired value for more than $\epsilon$ in absolute error are linearly penalized, otherwise are not considered errors. So, this parameter C works as a \textbf{regularization parameter}: on one hand if C is large, we give importance to the allowed errors; on the other hand we allows some misclassification. What we know about SVM models in general is the fact that they try to solve an optimization problem and one of the constraint is about allowing misclassification: the parameter C controls the number of allowed misclassification.
\\
The grid of values of C is in a range from 5 to 9. The best model is chosen according to the maximum explained variance. We can see the model behaviour in the following picture:

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{images/score.png}
	\caption{score of different model with different C}
	\label{fig:score of different models with different C}
\end{figure}

Note that in figure \ref{fig:score of different model with different C} the signature "SVR\_6" means that the model is trained with C=6.
\\ \\
According to this result, the best model is the one with C=9.
\\ \\
At this point the procedure for selecting the SVM model is finished. The final result of the model produced on the dataset defined in terms of MSE is the following:

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{images/SVR_result.png}
	\caption{SVR result}
	\label{fig:svr-result}
\end{figure}